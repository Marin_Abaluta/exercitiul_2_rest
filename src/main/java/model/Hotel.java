package model;

public class Hotel {
	
	private String name;
	private String rating;
	private Integer stars;
	private Integer id;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public Integer getStars() {
		return stars;
	}
	public void setStars(Integer stars) {
		this.stars = stars;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	public Hotel() {
		
		
	}
	
	public Hotel(Integer id, String name, String rating, Integer stars) {
		
		this.id = id;
		this.name = name;
		this.rating = rating;
		this.stars = stars;
	}
	
	@Override
	public String toString() {
		
		String infoHotel = String.format("Hotel Info: name = %s, rating = %s, stars =%d, id = %d",
                name, rating, stars, id);
		return infoHotel;
	}
}
