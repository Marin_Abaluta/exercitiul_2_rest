package controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import model.Hotel;
import model.PseudoDB;


@RestController
public class HotelController {
	
	
	public HotelController() {
		PseudoDB pseudoDb = new PseudoDB();
	}
	
	
	@RequestMapping(value = "/hotels", method = RequestMethod.GET)
	public List<Hotel> get() {
		System.out.println("Hoteluri " + PseudoDB.getList());
		return PseudoDB.getList();
	}
	
	
	@RequestMapping(value= "/hotels/{id}", method = RequestMethod.PUT)
	public String updateHotel(@PathVariable("id") Integer id, @RequestBody Hotel hotel) {
		
		List<Hotel> hotels = PseudoDB.getList();
		boolean status  = false;
		for (Hotel h : hotels)
		{
			//try catch
			if (id.equals(h.getId())) {
				
				if (!hotel.getStars().equals(null)) {
					
					h.setStars(hotel.getStars());
					status = true;
				}
			}
			
		}
		
		if (true == status) {
			
			return "A functionat!";
		}
		
		else 
			
			return "Mai incearca";
			
}
			

	

}
