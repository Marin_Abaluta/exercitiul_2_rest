package com.spring.requestbody;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import controller.HotelController;


@SpringBootApplication
@ComponentScan(basePackageClasses = HotelController.class)
public class Exercitiul2RestApplication {

	public static void main(String[] args) {
		SpringApplication.run(Exercitiul2RestApplication.class, args);
	}
}
